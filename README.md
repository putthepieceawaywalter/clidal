# CLIdal

#### CLIdal is a Linux command line interface client for the Tidal streaming service 
There aren't any native linux tidal clients that currently work so I made my own.  Most users will choose to just use the web browser, this is a hobby project.  Any pull requests are welcome!

### Navigation
- Arrow keys navigate. <br />
- When you're in the main screen you can use the 1-5 keys to navigate to the search, artist, album, song and currently playing windows respectively<br />

### Known issues
 - Entering nonsense in the search bar will cause an exception. Issue #49 investigates this
 - Tidal gives urls for songs that expire after 1 hour.  Pausing a song, waiting more than an hour and resuming causes an exception
 - This is a link that once was valid that can be used for testing that https://ab-pr-cf.audio.tidal.com/a0200f96fea455eb89ce46446fea0012_37.mp4?Expires=1673880671&Signature=G7nOcZzjW9Tn2GkTqqTLr~YHIvCKHrYxnEJqlm0wcScULxAkd6XTQVVzVscIqH63xRj~MSwGRCcAxYjEdjWBcQ6V4nBQRYddA~8RXPinl5jWWeUbvvtlYpXqLrNLeYHwNHQN6PT7OiPwmOcso1LFLJqJoZuNjWfJdKRfrla5VoLlu8VkSlwhTmL0ZSd9~aNxTYLiY9O4hViG0FDFIQE8-FDG1zdMUlgZgaMGOr0x06vU8MDp3dZaY6iPZjNZGbvF3or~o8AMHgJNURl2Udbne1r10Bzl3atoqFMRemrvVD94r3rWt3JzfqRBAmI0ZGtTEHZ1xPO6pAHCRWkXz2ZZ3Q__&Key-Pair-Id=K14LZCZ9QUI4JL




