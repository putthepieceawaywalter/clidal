# this class is used when the displayed text is wider than the window it appears in
# the scroll_text object will manage which part of  the string should be currently displayed

import datetime

class scroll_text():
    def __init__(self, text, width, interval):
        self.text = text
        self.width = width
        self.interval = interval
        self.most_recent_scroll = datetime.datetime.now()
        self.current_time = datetime.datetime.now()
        self.reset_time = 5
        self.index = 0
        self.finished = False
        self.time_finished = datetime.datetime.now()
    def delete(self):
        pass

    def scroll(self):
        if len(self.text) > self.width:
            if ((datetime.datetime.now() - self.most_recent_scroll).total_seconds() > self.interval) and self.index + self.width < len(self.text):
                self.index += 1
                self.most_recent_scroll = datetime.datetime.now()
        if self.index + self.width == len(self.text) - 1:
            self.finished = True
            self.time_finished = datetime.datetime.now()

        if self.finished:
            if (datetime.datetime.now() - self.time_finished).total_seconds() > self.reset_time:
                self.index = 0
                self.finished = False

        return self.text[self.index : self.index + self.width] 

    def get_text(self):
        return self.text[:self.width] 
                
         
