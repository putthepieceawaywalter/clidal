# this class serves as a wrapper around python-vlc

import vlc
import os
import sys
class media_player:
	def __init__(self):
		self.media_list_player = None
		self.media_player = None
		self.instance = vlc.Instance()
		os.environ['VLC_VERBOSE'] = '-1'
		#self.is_playing = False
		self.instance.log_unset()
		
	def __delete__(self):
		os.environ['VLC_VERBOSE'] = 0
	def play_list(self, songs, index=0):
		# attempts to play a list of songs, returns the finished index it is at if it gets stuck / finished
		finished_index = -1
		if self.media_list_player is not None:
			self.media_list_player.stop()	
		self.media_list_player = self.instance.media_list_player_new()
		media_list = self.instance.media_list_new(songs)
		self.media_list_player.set_media_list(media_list)
		try:
			self.media_list_player.play_item_at_index(index) 
			self.media_player = self.media_list_player.get_media_player()
		except Exception as ex:
			finished_index = self.media_player.get_current_track().id
			print("kjdfkjdfkjdf")
		return finished_index
	
	def play_pause(self):
		if self.media_list_player.is_playing() == True:
			self.media_player.pause()
		else:
			self.media_player.play()	

	def is_playing_or_paused(self):
		if self.media_list_player is not None:
			if self.media_list_player.get_state() != 'vlc.State.NothingSpecial':
				return True
		return False
	def next_track(self):
		self.media_list_player.next()
	def prev_track(self):
		self.media_list_player.previous()
	def fast_forward(self, time=5000):
		if self.media_player.get_time() + time < self.media_player.get_length():
			self.media_player.set_time(self.media_player.get_time() + time)
	def rewind(self, time=5000):
		if self.media_player.get_time() > time:
			self.media_player.set_time(self.media_player.get_time() - time)
	def get_current_track_url(self):
		# this will return the url or file path of the current playing song	
		try:
			return self.media_list_player.get_media_player().get_media().get_mrl()
		except:
			return None
	def get_current_track_string(self):
		if self.media_player is not None:
			return self.media_player.audio_get_track()
		return None
	def get_vlc_media_player(self):
		return self.media_player
	def get_track_time(self):
		try:
			return [self.media_player.get_time(), self.media_player.get_length()]
		except:
			return[-1. -1]
		
