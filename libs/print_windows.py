# these classes print the various windows in the application

import curses
from curses.textpad import Textbox, rectangle

about_str = "CLIdal is a cli client for the Tidal streaming service"

class media_win():
	def __init__(self, height, width, y, x, highlight, normal):
		self.highlight = highlight
		self.normal = normal
		self.win = curses.newwin(height, width, y, x)
		self.win_y = 1
		self.win_x = 1
		self.width = width
	def __delete__(self):
		pass
	def new_win(self, height, width, y, x):
		pass
	def refresh(self, names = None, selected_index = -1, top = None, bottom = None, default_text = None):
		self.win.erase()
		local_y = self.win_y
		if names is not None and len(names) != 0:
			if top is None or bottom is None:
				raise Exception("must pass top and bottom values when passing objects to the window")
			for i in range(top, bottom):
				if i == len(names) or local_y == bottom:
					break
				elif i == selected_index or type(i) == str:
					cp = self.highlight
					name = names[i].scroll()
				else:
					cp = self.normal
					name = names[i].get_text()
				self.win.addstr(local_y, self.win_x, name, cp)
				local_y += 1
		else:
			if default_text is None:
				default_text = about_str
			self.win.addstr(self.win_y, self.win_x, default_text) 

		self.win.border(0,0,0,0,0,0,0,0)
		self.win.refresh()
	
class player_win():
	def __init__(self, buttons, height, width, y, x, highlight, normal):
		self.player_buttons = buttons
		self.color_pair_highlight = curses.color_pair(1)
		self.color_pair_normal = curses.color_pair(2)
		self.button_width = 12
		self.win = curses.newwin(height, width, y, x)
		self.highlight = highlight
		self.normal = normal
	def __delete__(self):
		pass

	def refresh(self, song = None, selected_index = -1, curr_time = -1, total_time = -1):
		# takes song in as a string instead of a song object
		self.win.erase()
		playing_string = about_str
		time_string = None
		
		if song is not None:
			#raise Exception("song: " + song.name + '\n' + "total_time: " + total_time)
			playing_string = "Now Playing: " +  song.name + " by " + song.artist.name
			time_string = str(curr_time) + "/" + str(total_time)
			time_string.strip()
		self.win.addstr(1, 2, playing_string)
		if time_string is not None:
			self.win.addstr(2, 2, time_string)
		y = 3
		x = 2
		for index, column in enumerate(self.player_buttons):
			if index == selected_index:
				self.win.addstr(y, x, column, self.highlight)
			else:
				self.win.addstr(y, x, column, self.normal)
		
			x += self.button_width
		self.win.border(0,0,0,0,0,0,0,0)
		self.win.refresh()

def assign_enter_as_terminate(character):
	if character == 10:
		character = 7
	return character

class sb_win():
	def __init__(self, height, width, y, x, search_bar_width, text_height = 1, text_y = 3, text_x= 2):
		self.width = width
		self.height = height
		self.search_bar_width = search_bar_width
		self.text_width = self.search_bar_width - 2
		
		self.win = curses.newwin(self.height, self.width, y, x)
		self.win.border(0,0,0,0,0,0,0,0)
		self.text_win = curses.newwin(text_height, self.text_width, text_y, text_x)
		self.__set_textbox_size()
		self.rectangle = self.__rectangle()
		self.sb_box = None	

		# strings that are used in the search window
		self.nav_search_win = "1 - search window"
		self.nav_artist_win = "2 - artist window"
		self.nav_album_win = "3 - album window"
		self.nav_song_win = "4 - song window"
		self.nav_player_win = "5 - player window"
		self.max_nav_width = max(len(self.nav_search_win), len(self.nav_artist_win), len(self.nav_album_win), len(self.nav_song_win), len(self.nav_player_win)) 
		self.nav_buffer = 3
		
	def __delete__(self):
		pass
	def __rectangle(self):
		return rectangle(self.win, self.text_y - 1, self.text_x - 1, self.text_height + 3, self.text_width + 2)
	def __set_textbox_size(self):
		self.text_width = self.text_width
		self.text_height = 1
		self.text_y = 3
		self.text_x = 2

	def refresh(self):
		try:
			navigation_string = "1 - search window"+ '\n' + " + 2 - artist window'\n'3 - album window'\n'4 - song window'\n'5 - player window"
	
			self.win.erase()
			self.win.addstr(1, 2, "Enter text below to search")
			self.win.addstr(1, self.width - (self.max_nav_width + self.nav_buffer), self.nav_search_win)
			self.win.addstr(2, self.width - (self.max_nav_width + self.nav_buffer), self.nav_artist_win)
			self.win.addstr(3, self.width - (self.max_nav_width + self.nav_buffer), self.nav_album_win)
			self.win.addstr(4, self.width - (self.max_nav_width + self.nav_buffer), self.nav_song_win)
			self.win.addstr(5, self.width - (self.max_nav_width + self.nav_buffer), self.nav_player_win)
		except:
			raise Exception("Window not large enough, try maximizing terminal window")

		self.text_win.refresh()
		self.text_win = curses.newwin(self.text_height, self.text_width, self.text_y, self.text_x)
		self.sb_box = Textbox(self.text_win)
		self.win.border(0,0,0,0,0,0,0,0)
		self.rectangle = rectangle(self.win, 2, 1, 4, self.search_bar_width)
		self.win.refresh()
	def edit(self):
		self.sb_box.edit(assign_enter_as_terminate)
	def gather(self):
		return self.sb_box.gather()










