# this file contains two classes, clidal_api and clidal_data
# clidal_api is meant to serve as a wrapper around the python-tidalapi package
	# this class handles all interactions between this application and tidal such as logging in and searching for media
# clidal_data is meant to store and fetch data about the media the user is searching for

import tidalapi as ta
from pathlib import Path
import os
import csv
import threading

CLIDAL_PATH = '/.local/clidal_data/'

class clidal_api:
	def __init__(self):
		self.session = ta.Session()
		credentials_path = ""
		login_log_path = ""

	def login(self):
		# attempt to load oauth credentials
		return_message = ""	
		if self.session.check_login():
			return "already logged in"

		try:
			credentials_path = str(Path.home()) + CLIDAL_PATH + "credentials.txt"
			credentials = open(credentials_path, "r")
			cred_lines = credentials.readlines()
			cred_lines[0] = cred_lines[0].replace('\n', '')
			cred_lines[1] = cred_lines[1].replace('\n', '')			
			cred_lines[2] = cred_lines[2].replace('\n', '')
			cred_lines[3] = cred_lines[3].replace('\n', '')
			credentials.close()
			if (self.session.load_oauth_session(cred_lines[0], cred_lines[1], cred_lines[2], cred_lines[3])):
				return_message = "logged in successfully with saved credentials"
				
		except Exception as e:
			login_log_path = str(Path.home()) + CLIDAL_PATH + "login_log.log"
			f = open(login_log_path, "w")
			f.write(str(e))
			f.close()
			# login from loading credentials failed, offer user a link to login
			self.session.login_oauth_simple()
			
			# save credentials when done so user doesn't have to log in again
			
			credentials = open(credentials_path, "w")
			token_type = (self.session.token_type + '\n').replace(' ', '')
			access_token = (self.session.access_token + '\n').replace(' ','')
			refresh_token = (self.session.refresh_token + '\n').replace(' ', '')
			expiry_time = (str(self.session.expiry_time) + '\n').replace(' ', '')	
			write_success = False
			try:
				credentials.write(token_type)
				credentials.write(access_token)
				credentials.write(refresh_token)
				credentials.write(expiry_time)
				write_success = True
			except:
				return_message = "failed to save credentials for future logins"
			if (self.session.check_login() and write_success):
				return_message = "logged in successfully and saved credentials for future logins"	
			elif (self.session.check_login() and not write_success):
				return_message = "logged in successfully but unable to write log in credentials to disk"	
			else:
				return_message = "saved credentials did not work and unable to generate oauth link"
		return return_message

	def search(self, name, models = None, limit = 20):
		if models is None:
			models = [ta.artist.Artist, ta.album.Album, ta.media.Track]
		return self.session.search(name, models)	

	def get_artist_top_tracks(self, artist):
		return artist.get_top_tracks()	

	def get_artist_albums(self, artist):
		albums = []
		for album in artist.get_albums():
			albums.append(album)
		for album in artist.get_albums_ep_singles():
			albums.append(album)
		return albums

	def get_album_quality(self, album):
		track = album.tracks(limit=1)
		try:
			quality = track[0].audio_quality
			if str(quality) == 'Quality.master':
				quality = ' (Master)'
			elif str(quality) == 'Quality.lossless':
				quality = ' (Lossless)'
			elif str(quality) == 'Quality.high':
				quality = ' (High)'
			elif str(quality) == 'Quality.low':
				quality = ' (Low)'
			else:
				quality = ''	
		except:
			quality = ''
		return quality	
	
	def get_album_release_date(self, album):
		if type(album) == ta.album.Album:
			return " (" + str(album.available_release_date.year) + ")"
		return None

class clidal_data:
	def __init__(self):
		self.data_path = str(Path.home()) + "/.local/clidal_data/"
		if not os.path.isdir(self.data_path):
			os.mkdir(self.data_path)
		self.song_data_path = self.data_path +  "song_data.txt"
		self.album_data_path = self.data_path + "album_data.txt"
		self.songs = None

	def __delete__(self):
		try:
			self.song_data_file.close()
			self.album_data_file.close()
		except:
			pass

	def set_local_track_data(self, ids, urls):
		self.song_data_file = open(self.song_data_path, "w")
		if len(ids) != len(urls):
			raise Exception("can't write different amount of ids and urls")
		for i in range(len(ids)):
			self.song_data_file.write(str(ids[i]) + ', ' + urls[i] + '\n')
		self.song_data_file.close()	

	def get_track_id_from_url(self, vlc_url): # if vlc_url is in song data, return the id
		self.song_data_file = open(self.song_data_path, "r")
		csv_reader = csv.reader(self.song_data_file)
		song_data = list(csv_reader)
		result = None

		for line in song_data:
			if line[1].strip() == vlc_url:
				result = line[0]
				break	
				
		self.song_data_file.close()
		return result
	def set_songs(self, songs):
		self.songs = songs

	def get_album_quality(self, album):
		quality = ""	
		try:
			self.album_data_file = open(self.album_data_path, "r")
		except FileNotFoundError:
			# file does not exist yet so create it
			self.album_data_file = open(self.album_data_path, "w")
			self.album_data_file.close()
			self.album_data_file = open(self.album_data_path, "r")

		csv_reader = csv.reader(self.album_data_file)
		album_data = list(csv_reader)
		self.album_data_file.close()
		ids = []

		for i in range(len(album_data)):
			ids.append(album_data[i][0])
		
		album_index = 0
		for id in ids:
			if str(album.id) == id:
				break
			else:
				album_index += 1
			
		if album_index < len(ids):
			quality = album_data[album_index][1]
		else:
			track = album.tracks(limit=1)
			try:
				quality = track[0].audio_quality
				if str(quality) == 'Quality.master':
					quality = ' (M)'
				elif str(quality) == 'Quality.lossless':
					quality = ' (L)'
				elif str(quality) == 'Quality.high':
					quality = ' (High)'
				elif str(quality) == 'Quality.low':
					quality = ' (Low)'
				else:
					quality = ''	
			except:
				quality = ''
			# write it to the disk for next time
			self.album_data_file = open(self.album_data_path, "a")
			self.album_data_file.write(str(album.id) + ',' + quality + '\n')
			self.album_data_file.close()		
		return quality
	def get_current_track(self, url):
		# returns a song object that matches the url
		id = self.get_track_id_from_url(url)	
		for song in self.songs:
			if str(song.id) == id:
				return song
		return None

		

