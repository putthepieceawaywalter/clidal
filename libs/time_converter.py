# I couldn't find an easy way to convert milliseconds to minutes seconds
# this makes the conversion and formats the way clidal expexts
class time_converter:
	def __init__(self):
		pass
	def milli_to_min_sec(self, milliseconds):
		total_seconds = milliseconds / 1000
		minutes, seconds = divmod(total_seconds, 60)
		
		# formatting the string
		if minutes < 10:
			minutes = '0' + str(int(minutes))
		else:
			minutes = str(int(minutes))
		if seconds < 10:
			seconds = '0' + str(int(seconds))
		else:
			seconds = str(int(seconds))
		return str(minutes) + ':' + str(seconds)
		
