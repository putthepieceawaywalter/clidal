# This class manages the media windows.  The media windows are essentially 3 x,y grids next to each other

class media_grid_data():
    def __init__(self, media_bottoms, media_window_bottom, media_list_size, media_rows = [0, -1, -1], media_tops = [0, 0, 0]):
        self.media_bottoms = media_bottoms
        self.media_window_bottom = media_window_bottom 
        self.media_rows = media_rows
        self.media_tops = media_tops
        self.media_wins_column = 0
        self.total_wins = 3
        self.media_list_size = media_list_size
        self.artist_column = 0
        self.album_column = 1
        self.song_column = 2

    def __delete__(self):
        pass
    def increment_media_win(self):
        # Select media window to the right of the currently highlighted window 
        if self.media_wins_column < self.total_wins - 1:
            self.media_wins_column += 1
            self.reset_rows()
    def decrement_media_win(self):
        # Select media window to the left of the currently highlighted window 
        if self.media_wins_column > 0:
            self.media_wins_column -= 1
            self.reset_rows()
    def increment_row(self):
        # User is trying to move cursor down
        if self.media_rows[self.media_wins_column] < self.media_list_size[self.media_wins_column] - 1:
            self.media_rows[self.media_wins_column] += 1
            if self.media_rows[self.media_wins_column] == self.media_bottoms[self.media_wins_column]:
                self.media_tops[self.media_wins_column] += 1
                self.media_bottoms[self.media_wins_column] += 1

    def decrement_row(self):
        # User is trying to move cursor up
        # TODO this is currently not preventing the user from running over the end of the media _list
        if self.media_rows[self.media_wins_column] > 0:
            self.media_rows[self.media_wins_column] -= 1
            if self.media_rows[self.media_wins_column] < self.media_tops[self.media_wins_column]:
                self.media_tops[self.media_wins_column] -= 1
                self.media_bottoms[self.media_wins_column] -= 1
    def reset_rows(self):        
        for i in range(len(self.media_tops)):
            self.media_tops[i] = 0
        for i in range(len(self.media_bottoms)):
            self.media_bottoms[i] = self.media_window_bottom
        for i in range(len(self.media_rows)):
            if i == self.media_wins_column:
                self.media_rows[i] = 0
            else:
                self.media_rows[i] = -1
    
    def change_to_artist_window(self):
        # change to artist window and update other stuff accordingly
        self.media_wins_column = self.artist_column
        self.reset_rows()
    def change_to_album_window(self):
        self.media_wins_column = self.album_column
        self.reset_rows()
    def change_to_song_window(self):
        self.media_wins_column = self.song_column
        self.reset_rows()
    def change_to_search_window(self):
        self.media_wins_column = -1 
        self.reset_rows()
    def change_to_player_window(self):
        self.media_wins_column = -1
        self.reset_rows()
        
    def print(self):
        print("media_rows: " + str(self.media_rows))
        print("media_tops: " + str(self.media_tops))
        print("media_bottoms: " + str(self.media_bottoms))
        print("media_wins_column: " + str(self.media_wins_column))

